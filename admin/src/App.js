import React, { Component } from 'react';
import Tabs from 'react-toolbox/lib/tabs/Tabs';
import Tab from 'react-toolbox/lib/tabs/Tab';
import AppBar from 'react-toolbox/lib/app_bar/AppBar';
import Layout from 'react-toolbox/lib/layout/Layout';
import 'animate.css/animate.css'
import sa from 'superagent';

import _ from 'lodash';

import 'material-design-icons/iconfont/material-icons.css';
import 'material-design-icons/iconfont/MaterialIcons-Regular.eot';

import logo from './cb.png';
import './App.css';

import DeviceAdmin from './DeviceAdmin';
import TypeAdmin from './TypeAdmin';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { index: 0 };
  }

  componentDidMount() {
    this.update();
  }
  update() {
    this.setState({ updatingTypes: true, updatingDevices: true });
    sa.get('/api/types')
    .end((err, res) => {
      if (err || !res.ok) {
        this.setState({ updatingTypes: false, error: _.get(res, 'error.message') });
      } else {
        this.setState({ updatingTypes: false, types: res.body });
      }
    });
    sa.get('/api/devices')
    .end((err, res) => {
      if (err || !res.ok) {
        this.setState({ updatingDevices: false, error: _.get(res, 'error.message') });
      } else {
        this.setState({ updatingDevices: false, devices: res.body });
      }
    });
  }
  renderIcon() {
    return (<img src={logo} className="App-logo" alt="logo" />);
  }
  render() {
    return (
      <div className="App">
        <AppBar className="App-header" title="Device Controller Admin"
          rightIcon={this.renderIcon()}>
        </AppBar>
        <Layout>
        <Tabs index={this.state.index} onChange={(index) => { this.setState({index}); }} fixed>
          <Tab label="Devices">
            <DeviceAdmin onUpdate={this.update.bind(this)}
              types={this.state.types} devices={this.state.devices}
              updating={this.state.updatingTypes || this.state.updatingDevices} />
          </Tab>
          <Tab label="Types">
            <TypeAdmin onUpdate={this.update.bind(this)}
              types={this.state.types} updating={this.state.updatingTypes} />
          </Tab>
        </Tabs>
        </Layout>
      </div>
    );
  }
}

export default App;
