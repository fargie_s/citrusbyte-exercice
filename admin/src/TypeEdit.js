import React, { Component } from 'react';
import _ from 'lodash';

import List from 'react-toolbox/lib/list/List';
import ListItem from 'react-toolbox/lib/list/ListItem';

class TypeEdit extends Component {
  constructor(props) {
    super(props);
    this.state = { type: _.cloneDeep(props.type) };
  }

  componentWillReceiveProps(nextProps) {
    if (_.has(nextProps, 'type')) {
      this.setState({ type: _.cloneDeep(nextProps.type) });
    }
  }

  renderControls() {
    return _.map(_.get(this.state, 'type.properties'), (prop, id) => {
      return (
        <ListItem
          caption={id} legend={prop.control}
          leftIcon={prop.icon} />
      )
    });
  }
  render() {
    return (
      <List>
        {this.renderControls()}
      </List>
    );
  }
}

export default TypeEdit;
