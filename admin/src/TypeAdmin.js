import React, { Component } from 'react';
import _ from 'lodash';

import Layout from 'react-toolbox/lib/layout/Layout';
import Panel from 'react-toolbox/lib/layout/Panel';
import Sidebar from 'react-toolbox/lib/layout/Sidebar';
import List from 'react-toolbox/lib/list/List';
import ListItem from 'react-toolbox/lib/list/ListItem';
import IconButton from 'react-toolbox/lib/button/IconButton';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';

import 'animate.css/animate.css'

import { Row, Col } from 'react-flexbox-grid';

import TypeEdit from './TypeEdit';

class DeviceAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = { selected: null, isMod: false, types: _.cloneDeep(props.types) };
  }

  componentWillReceiveProps(nextProps) {
    if (_.has(nextProps, 'types')) {
      this.setState({ selected: null, isMod: false, types: _.cloneDeep(nextProps.types) });
    }
  }
  fetch() {
    _.attempt(this.props.onUpdate);
  }

  render() {
    return (
      <Layout>
        <Panel className="panel">
          <List style={{ flex: 1 }} selectable ripple>{this.renderList()}</List>
          <Row bottom="xs" end="md" center="xs">
            <Col xs>
              <IconButton className={"action-btn " + (this.props.updating ? "spinner" : "")} icon="loop" accent
                onClick={this.fetch.bind(this)} />
            </Col>
          </Row>
        </Panel>
        <Sidebar className="sidebar" pinned={!_.isNil(this.state.selected)} width={66}>
          <div><IconButton icon='close' onClick={this.setState.bind(this, {selected: null}, null)} accent /></div>
          <div style={{ flex: 1 }}>
            <TypeEdit
              type={_.get(this.state, [ 'types', this.state.selected ])} />
          </div>
        </Sidebar>
      </Layout>
    );
  }
  renderList() {
    return _.map(_.get(this.state, 'types'),
      (type, id) => {
        return (
          <ListItem key={id}
            leftIcon={<FontIcon className={(this.state.selected === id) ? "highlight" : ""}
              value={type.icon} />}
            caption={id}
            onClick={this.setState.bind(this, {selected: id}, null)} />
        );
      }
    );
  }
}

export default DeviceAdmin;
