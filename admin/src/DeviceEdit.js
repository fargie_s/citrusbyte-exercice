import React, { Component } from 'react';
import _ from 'lodash';

import Checkbox from 'react-toolbox/lib/checkbox/Checkbox';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';
import Input from 'react-toolbox/lib/input/Input';
import List from 'react-toolbox/lib/list/List';

import Dropdown from 'react-toolbox/lib/dropdown/Dropdown';
import Slider from 'react-toolbox/lib/slider/Slider';
import Switch from 'react-toolbox/lib/switch/Switch';

import { Row, Col } from 'react-flexbox-grid';

import SelectEdit from './SelectEdit';

class DeviceEdit extends Component {
  constructor(props) {
    super(props);
    this.state = { device: _.cloneDeep(props.device) };
  }

  componentWillReceiveProps(nextProps) {
    if (_.has(nextProps, 'device')) {
      this.setState({ device: _.cloneDeep(nextProps.device) });
    }
  }

  propChange(name, value) {
    this.setState((state) => {
      state.device[name] = value;
      _.attempt(this.props.onChange, state.device);
      return state;
    });
  }
  typeChange(type) {
    this.setState((state, props) => {
      state.device = {
        name: 'My ' + type,
        type: type,
        uri: '',
        properties: {}
      };
      _.forEach(_.get(props, [ 'types', type, 'properties' ]),
        function(prop, name) {
          state.device.properties[name] = { name: name };
        }
      );
      _.attempt(this.props.onChange, state.device);
      return state;
    });
  }
  controlToggle(type, enabled) {
    this.setState((state, props) => {
      if (enabled) {
        state.device.properties[type] = { name: type };
      } else {
        _.unset(state.device, [ 'properties', type ]);
      }
      _.attempt(this.props.onChange, state.device);
      return state;
    });
  }
  controlNameChange(propId, name) {
    this.setState((state) => {
      _.set(state.device, [ 'properties', propId, 'name' ], name);
      _.attempt(this.props.onChange, state.device);
      return state;
    });
  }
  controlSourceChange(propId, source) {
    this.setState((state) => {
      _.set(state.device, [ 'properties', propId, 'source' ], source);
      _.attempt(this.props.onChange, state.device);
      return state;
    });
  }

  renderControlValue(propId, control, prop) {
    if (control === 'Button') {
      return (<Switch disabled checked={prop.value} />);
    } else if (control === 'Slider') {
      return (<Slider disabled value={prop.value} />);
    } else if (control === 'Select') {
      return (<SelectEdit source={prop.source} value={prop.value}
        onChange={this.controlSourceChange.bind(this, propId)} />);
    }
  }
  renderControlEdit(propId, desc, prop) {
    return (
      <Row middle="xs">
        <Col xs={4}>
          <Input type="text" disabled={_.isNil(prop)}
            onChange={this.controlNameChange.bind(this, propId)} value={_.get(prop, 'name', propId)}/>
        </Col>
        <Col xs={8}>{!_.isNil(prop) && this.renderControlValue(propId, desc.control, prop)}</Col>
      </Row>);
  }
  renderControls() {
    if (!this.state.device) {
      return;
    }
    var type = _.get(this.props, ['types', this.state.device.type]);
    if (!type) {
      return;
    }
    return _.map(type.properties, (propDesc, propId) => {
      var prop = _.get(this.state, [ 'device', 'properties', propId ]);
      return (
        <Row key={propId} middle="xs">
          <Col xs={1}><Row end="xs"><FontIcon value={propDesc.icon} /></Row></Col>
          <Col xs>{this.renderControlEdit(propId, propDesc, prop)}</Col>
          <Col xs={1}><Checkbox onChange={this.controlToggle.bind(this, propId)} checked={!_.isNil(prop)} /></Col>
        </Row>
      );
    });
  }
  render() {
    return (
      <div>
        <Dropdown
          label="Type"
          source={_.map(this.props.types, (type, name) => { return { label: name, value: name }; })}
          value={_.get(this.state, 'device.type')}
          onChange={this.typeChange.bind(this)} />
        <Input type='text' label='Name' value={_.get(this.state, 'device.name')} onChange={this.propChange.bind(this, 'name')} />
        <Input type='text' label='URI' value={_.get(this.state, 'device.uri')} onChange={this.propChange.bind(this, 'uri')} />
        <List>
        {this.renderControls()}
        </List>
      </div>
    );
  }
}

export default DeviceEdit;
