import React, { Component } from 'react';
import _ from 'lodash';

import Dropdown from 'react-toolbox/lib/dropdown/Dropdown';
import AutoComplete from 'react-toolbox/lib/autocomplete/Autocomplete';

class SelectEdit extends Component {
  constructor(props) {
    super(props);
    this.state = { source: props.source, editing: false };
  }

  onChange(value) {
    _.attempt(this.props.onChange, value);
  }
  render() {
    if (this.state.editing) {
      return (<AutoComplete source={this.props.source} value={this.props.source}
        allowCreate={true}
        onChange={this.onChange.bind(this)} autoFocus />);
    } else {
      return (
        <div onClick={this.setState.bind(this, { editing: true }, null)}>
          <Dropdown disabled
            label="Click to edit"
            source={[{value: this.props.value, label: this.props.value}]}
            value={this.props.value} />
        </div>
      )
    }
  }
}

export default SelectEdit;
