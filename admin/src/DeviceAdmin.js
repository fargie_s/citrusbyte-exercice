import React, { Component } from 'react';
import _ from 'lodash';

import Layout from 'react-toolbox/lib/layout/Layout';
import Panel from 'react-toolbox/lib/layout/Panel';
import Sidebar from 'react-toolbox/lib/layout/Sidebar';
import List from 'react-toolbox/lib/list/List';
import ListItem from 'react-toolbox/lib/list/ListItem';
import Button from 'react-toolbox/lib/button/Button';
import IconButton from 'react-toolbox/lib/button/IconButton';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';

import sa from 'superagent';

import 'animate.css/animate.css'

import { Row, Col } from 'react-flexbox-grid';

import DeviceEdit from './DeviceEdit';
import Error from './Error';

class DeviceAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = { selected: null, isMod: false, devices: _.cloneDeep(props.devices) };
  }

  componentWillReceiveProps(nextProps) {
    if (_.has(nextProps, 'devices')) {
      this.setState({ selected: null, isMod: false, devices: _.cloneDeep(nextProps.devices) });
    }
  }
  fetch() {
    _.attempt(this.props.onUpdate);
  }
  addDevice() {
    this.setState((state) => {
      var devices = _.get(state, 'devices', {});
      var id = _.toString((_.last(_.sortBy(
        _.map(_.keys(devices), (k) => { return _.toInteger(k); }))) || 0) + 1);
      devices[id] = {};
      state.devices = devices;
      state.selected = id;
      state.isMod = true;
      return state;
    });
  }
  removeDevice(id) {
    this.setState((state) => {
      _.unset(state.devices, id);
      state.selected = null;
      state.isMod = true;
      return state;
    });
  }
  deviceChange(id, device) {
    this.setState((state) => {
      state.devices[id] = device;
      state.isMod = true;
      return state;
    });
  }
  upload() {
    this.setState({ selected: null });
    sa.post('/api/devices')
    .send(_.omitBy(this.state.devices, _.isEmpty))
    .end((err, res) => {
      if (err || !res.ok) {
        this.setState({ error: _.get(res, 'error.message') });
      } else {
        this.setState({ isMod: false });
        _.attempt(this.props.onUpdate);
      }
    });
  }

  render() {
    return (
      <Layout>
        <Panel className="panel">
          <List style={{ flex: 1 }} selectable ripple>{this.renderList()}</List>
          <Row bottom="xs" end="md" center="xs">
            <Col xs={1}>
            <div className={this.state.isMod ? "animated fadeIn" : "hidden"}>
              <Button className="pulse animated action-btn infinite"
                icon="file_upload" floating accent mini
                onClick={this.upload.bind(this)} />
            </div>
            </Col>
            <Col xs>
              <IconButton className={"action-btn " + (this.props.updating ? "spinner" : "")} icon="loop" accent
                onClick={this.fetch.bind(this)} />
              <Button className="action-btn" icon="add" floating accent
                onClick={this.addDevice.bind(this)} />
            </Col>
          </Row>
          <Error error={this.state.error} />
        </Panel>
        <Sidebar className="sidebar" pinned={!_.isNil(this.state.selected)} width={66}>
          <div><IconButton icon='close' onClick={this.setState.bind(this, {selected: null}, null)} accent /></div>
          <div style={{ flex: 1 }}>
            <DeviceEdit
              types={this.props.types}
              device={_.get(this.state, [ 'devices', this.state.selected ])}
              onChange={this.deviceChange.bind(this, this.state.selected)} />
          </div>
        </Sidebar>
      </Layout>
    );
  }

  renderList() {
    return _.map(_.get(this.state, 'devices'),
      (dev, id) => {
        return (
          <ListItem key={id}
            leftIcon={<FontIcon className={(this.state.selected === id) ? "highlight" : ""}
              value={_.get(this.props, ['types', dev.type, 'icon'], "")} />}
            caption={dev.name} legend={dev.type}
            onClick={this.setState.bind(this, {selected: id}, null)}
            rightIcon={<Button icon="remove" floating mini
              onClick={(e) => {
                this.removeDevice(id);
                e.stopPropagation();
              }} />}
            />
        );
      }
    );
  }
}

export default DeviceAdmin;
