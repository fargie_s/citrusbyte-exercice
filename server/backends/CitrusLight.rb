
require 'net/http'
require 'uri'

class CitrusLight
    @@type = 'Citrus Lights'

    def self.type
        @@type
    end

    def CitrusLight.update(device, properties)
        tries = 5
        ret = false
        return if not device.has_key? 'uri'
        begin
            puts "requesting " + device['uri']
            res = Net::HTTP.post URI(device['uri']),
                properties['On/Off']['value'].to_json,
                "Content-Type" => "application/json"
            puts res.inspect
            raise (res.body.strip || "Invalid reply from device: %i" % res.code) unless res.code == '202'
            ret = true
        rescue
            puts $!.message
            tries -= 1
            retry unless tries <= 0
            raise $!.message
        end
        return ret
    end
end
