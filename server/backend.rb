
require './backends/CitrusLight.rb'

class Backend
    def Backend.update(device, values)
        if device['type'] == CitrusLight.type then
            return CitrusLight.update(device, values)
        else
            return true
        end
    end
end
