ENV['RACK_ENV'] = 'test'

require './device_server.rb'  # <-- your sinatra app
require 'test/unit'
require 'rack/test'
require 'json'

class DeviceServerTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def app
        Sinatra::Application
    end

    def test_root
        get '/'
        assert last_response.ok?
    end

    def test_devices
        get '/api/devices'
        assert last_response.ok?
        json = JSON.parse last_response.body
        assert_equal 3, json.length
    end

    def test_types
        get '/api/types'
        assert last_response.ok?
        json = JSON.parse last_response.body
        assert_equal 4, json.length
    end

    def test_update
        post '/api/0/values', { "Power" => { "value" => false } }.to_json , "CONTENT_TYPE" => "application/json"
        assert last_response.ok?
        get '/api/devices'
        assert last_response.ok?
        json = JSON.parse last_response.body
        assert_equal false, json['0']['properties']['Power']['value']
    end

    def test_update_error
        post '/api/0/values', "invalid".to_json, "CONTENT_TYPE" => "application/json"
        assert !last_response.ok?
    end
end
