require 'sinatra'
require 'json'
require './backend.rb'

STATIC_SITE = File.dirname(__FILE__) + '/../build'
set :public_folder, STATIC_SITE
set :port, 8080

devices = JSON.parse File.read('devices.json')
types = JSON.parse File.read('types.json')

get '/' do
    File.read(STATIC_SITE + '/index.html')
end

get '/admin/?' do
    File.read(STATIC_SITE + '/admin/index.html')
end

get '/api/devices' do
    content_type :json
    devices.to_json
end

get '/api/types' do
    content_type :json
    types.to_json
end

post '/api/device/:device/values' do
    request.body.rewind
    values = JSON.parse request.body.read
    if not values.is_a? Hash
        halt 500, "Invalid parameter"
    elsif not devices.has_key? params['device'] then
        halt 404, "No such device"
    else
        begin
            if not Backend.update devices[params['device']], values then
                halt 500, "Failed to update device"
            else
                dev = devices[params['device']]
                devProps = dev['properties']
                values.each_pair { |k, v| devProps[k].merge! v if devProps.has_key? k }
                return nil
            end
        rescue
            halt 500, $!.message
        end
    end
end

post '/api/devices' do
    request.body.rewind
    values = JSON.parse request.body.read
    if not values.is_a? Hash
        halt 500, "Invalid parameter"
    else
        devices = values
        return nil
    end
end

get '/api/device/:device' do
    if devices.has_key? params['device'] then
        content_type :json
        return devices[params['device']].to_json
    else
        halt 404, "No such device"
    end
end

post '/api/types' do
    request.body.rewind
    body = request.body.read
    types = JSON.parse body # just check that it can be parsed
end
