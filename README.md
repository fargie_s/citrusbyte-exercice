
# CitrusByte exercice app

## Building

Ruby, rake, gem, npm and node are required dependencies to build and run this application.

To build this app run:
```bash
# installs ruby and node dependencies
npm run install_all

# build the front-end
npm run build_all
```

## Running

Once built the server can be run with:
```bash
cd server
rake run
```

**Note**: the website is then available on <http://localhost:8080>


## Debugging

To run a run and debug the front-end:
```bash
npm run start
```

**Note:**: A react-scripts server will then run on <http://localhost:3000>
