import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AppBar from 'react-toolbox/lib/app_bar/AppBar';
import Layout from 'react-toolbox/lib/layout/Layout';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';

import 'animate.css/animate.css'
import sa from 'superagent';

import _ from 'lodash';

import 'material-design-icons/iconfont/material-icons.css';
import 'material-design-icons/iconfont/MaterialIcons-Regular.eot';

import logo from './cb.png';
import './App.css';

import Transition from './Transition';
import DeviceList from './DeviceList';
import Device from './Device';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }


  componentDidMount() {
    sa.get('/api/types')
    .end((err, res) => {
      if (err || !res.ok) {
        this.setState({ error: _.get(res, 'error.message') });
      } else {
        this.setState({ types: res.body });
      }
    });
    sa.get('/api/devices')
    .end((err, res) => {
      if (err || !res.ok) {
        this.setState({ error: _.get(res, 'error.message') });
      } else {
        this.setState({ devices: res.body });
      }
    });
  }
  renderIcon() {
    return (<img src={logo} className="App-logo" alt="logo" />);
  }
  renderLeftIcon() {
    return (<FontIcon value="chevron_left" disabled className={(this.props.location.pathname !== '/') ? '' : 'hidden'}/>);
  }
  render() {
    return (
      <div className="App">
        <AppBar className="App-header" title="Device Controller"
          rightIcon={this.renderIcon()}
          leftIcon={this.renderLeftIcon()}
          onLeftIconClick={() => {this.context.router.history.push('/');}}>
        </AppBar>
        <Layout>
        <Transition pathname={this.props.location.pathname}>
          <Switch key={this.props.location.key} location={this.props.location}>
            <Route exact path="/" render={() => (<DeviceList types={this.state.types} devices={this.state.devices} />)} />
            <Route path="/:device" render={(props) => (<Device types={this.state.types} params={props.match.params} />)} />
          </Switch>
        </Transition>
        </Layout>
      </div>
    );
  }
}

App.propTypes = {
  route: React.PropTypes.object,
  location: React.PropTypes.object
};

App.contextTypes = {
  router: React.PropTypes.object
};

export default App;
