import React, { Component } from 'react';
import Panel from 'react-toolbox/lib/layout/Panel';
import List from 'react-toolbox/lib/list/List';
import ListSubHeader from 'react-toolbox/lib/list/ListSubHeader';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';

import _ from 'lodash';
import d from 'debug';
import sa from 'superagent';

import Button from './controls/Button';
import Slider from './controls/Slider';
import Select from './controls/Select';
import Error from './Error';

var debug = d('app:device');

class Device extends Component {
  constructor(props) {
    super(props);
    this.state = { device: null, type: null };
  }

  componentWillReceiveProps(nextProps) {
    if (_.has(nextProps, 'params.device')) {
      this.fetch();
    }
  }
  componentDidMount() {
    this.fetch();
  }
  fetch() {
    this.setState({ 'device': null });
    sa.get('/api/device/' + this.props.params.device)
    .end((err, res) => {
      if (err || !res.ok) {
        debug(err);
        this.setState({ 'error': _.get(res, 'error.message') });
      } else {
        var device = res.body;
        this.setState({
          type: _.get(this.props, ['types', _.get(device, 'type')]),
          device: device
        });
      }
    });
  }
  update(prop, value) {
    this.setState({ 'updating': true });
    sa.post('/api/device/' +  + this.props.params.device + '/values')
    .send({ [prop]: { value: value }})
    .end((err, res) => {
      if (err || !res.ok) {
        this.setState({
          'updating': false,
          'error': res.text || _.get(res, 'error.message')
        });
        this.fetch();
      } else {
        this.setState((prevState) => {
          prevState.updating = false;
          prevState.error = null;
          _.assign(prevState.device.properties[prop], { value: value });
          return prevState;
        });
      }
    });
  }
  renderControls(device, type) {
    if (!device || !type) {
      return;
    }
    return _.map(device.properties, (prop, propType) => {
      var propDesc = _.get(type, ['properties', propType]);
      if (!propDesc) {
        debug('unknown property', prop);
        return;
      }
      var control = Device.Controls[propDesc.control];
      if (_.isNil(control)) {
        debug('invalid control');
        return;
      }
      return React.createElement(control, _.assign({
        className: "control", key: prop.name, icon: propDesc.icon,
        onChange: this.update.bind(this, prop.name) }, prop));
    });
  }
  render() {
    return (
      <Panel className="page">
        <List>
          <ListSubHeader caption={_.get(this.state, 'device.name')} />
          {this.renderControls(this.state.device, this.state.type)}
        </List>
        {this.state.updating && <div className="center"><FontIcon className="spinner" value="loop" /></div>}
        <Error error={this.state.error} />
      </Panel>
    );
  }
}

Device.contextTypes = {
  router: React.PropTypes.object
};

Device.Controls = {
  "Button": Button,
  "Slider": Slider,
  "Select": Select
}

export default Device;
