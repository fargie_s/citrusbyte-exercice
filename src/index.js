import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {
  HashRouter as Router, Route } from 'react-router-dom';
// import { Router, Route } from 'react-router';

import "./toolbox/theme.css";
import theme from './toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';


ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </ThemeProvider>,
document.getElementById('root'));
registerServiceWorker();
