import React, { Component } from 'react';

import { RouteTransition } from 'react-router-transition';
import spring from 'react-motion/lib/spring';

const slideConfig = { stiffness: 200, damping: 30 };
const slideLeft = {
  atEnter: {
    opacity: 0,
    offset: 100,
  },
  atLeave: {
    opacity: spring(0, slideConfig),
    offset: spring(-100, slideConfig),
  },
  atActive: {
    opacity: spring(1, slideConfig),
    offset: spring(0, slideConfig),
  },
  mapStyles(styles) {
    return {
      opacity: styles.opacity,
      transform: `translateX(${styles.offset}%)`,
    };
  },
};

class Transition extends Component {
  render() {
    return (
      <RouteTransition
        className="transition-wrapper"
        pathname={this.props.pathname}
        {...slideLeft}>
        {this.props.children}
      </RouteTransition>
    );
  }
}

export default Transition;
