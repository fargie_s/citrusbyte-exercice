import React, { Component } from 'react';
import _ from 'lodash';

import Panel from 'react-toolbox/lib/layout/Panel';
import List from 'react-toolbox/lib/list/List';
import ListItem from 'react-toolbox/lib/list/ListItem';
import ListSubHeader from 'react-toolbox/lib/list/ListSubHeader';

class DeviceList extends Component {
  render() {
    return (
      <Panel className="page">
        <List selectable ripple>
          <ListSubHeader caption="Devices" />
          {this.renderList()}
        </List>
      </Panel>
    );
  }

  renderList() {
    return _.map(_.get(this.props, 'devices'),
      (dev, id) => {
        return (
          <ListItem key={id}
            leftIcon={_.get(this.props, ['types', dev.type, 'icon'], "")} caption={dev.name} legend={dev.type}
            onClick={ () => {
              this.context.router.history.push('/' + id) }
            }/>
        );
      }
    );
  }
}

DeviceList.contextTypes = {
  router: React.PropTypes.object
};

export default DeviceList;
