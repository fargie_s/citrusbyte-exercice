import React, { Component } from 'react';
import Dropdown from 'react-toolbox/lib/dropdown/Dropdown';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';
import { Grid, Row, Col } from 'react-flexbox-grid';

import _ from 'lodash';

class Select extends Component {
  constructor(props) {
    super(props);
    this.state = { value: props.value };
  }

  renderIcon() {
    return _.has(this.props, "icon") ? (<FontIcon className="icon" value={this.props.icon} />) : undefined;
  }
  render() {
    return (
      <Row middle="xs" className={this.props.className}>
      <Col xs={3} sm={2} md={1}>{this.renderIcon()} <div className="name">{this.props.name}</div></Col>
      <Col xs>
      <Dropdown
        source={_.map(this.props.source, (s) => { return {value: s, label: s}; })}
        auto={false}
        value={this.state.value}
        onChange={(value) => {
          /* FIXME: update value and reflect UI accordingly */
          this.setState({ value: value });
          _.attempt(this.props.onChange, value);
        }}
        />
      </Col>
      </Row>
    );
  }
}

export default Select;
