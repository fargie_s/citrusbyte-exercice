import React, { Component } from 'react';

import _ from 'lodash';

class Control extends Component {
  constructor(props) {
    super(props);
    this.state = { value: props.value };
  }
}

export default Control;
