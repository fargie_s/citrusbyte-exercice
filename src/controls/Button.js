import React, { Component } from 'react';
import Switch from 'react-toolbox/lib/switch/Switch';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';
import { Grid, Row, Col } from 'react-flexbox-grid';

import _ from 'lodash';

import Control from './Control';

class Button extends Control {
  renderIcon() {
    return _.has(this.props, "icon") ?
      (<FontIcon className="icon" value={this.props.icon} />) : undefined ;
  }
  render() {
    return (
      <Row middle="xs" className={this.props.className}>
      <Col xs={3} sm={2} md={1}>{this.renderIcon()} <div className="name">
        {this.props.name}</div></Col>
      <Col xs>
        <Switch className="button" checked={this.state.value}
          onChange={(value) => {
            this.setState({ value: value });
            _.attempt(this.props.onChange, value);
          }}
        />
      </Col>
      </Row>
    );
  }
}

export default Button;
