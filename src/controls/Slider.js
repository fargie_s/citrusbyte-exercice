import React, { Component } from 'react';
import ReactSlider from 'react-toolbox/lib/slider/Slider';
import FontIcon from 'react-toolbox/lib/font_icon/FontIcon';
import { Grid, Row, Col } from 'react-flexbox-grid';

import _ from 'lodash';

import Control from './Control';

class Slider extends Control {
  constructor(props) {
    super(props);
    this.state = { value: props.value };
  }

  renderIcon() {
    return _.has(this.props, 'icon') ? (<FontIcon className="icon" value={this.props.icon} />) : undefined;
  }
  render() {
    return (
      <Row start="xs" middle="xs" className={this.props.className}>
      <Col xs={3} sm={2} md={1}>{this.renderIcon()} <div className="name">{this.props.name}</div></Col>
      <Col xs>
        <ReactSlider className="slider" value={this.state.value}
          onChange={(value) => { this.setState({ value: value }); }}
          onDragStop={() => {
            _.attempt(this.props.onChange, this.state.value);
          }}
        />
      </Col>
      </Row>
    );
  }
}

export default Slider;
