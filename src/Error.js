import React, { Component } from 'react';
import _ from 'lodash';


class Error extends Component {
  render() {
    if (!_.isNil(this.props.error)) {
      return (<div className="error center">[{this.props.error}]</div>);
    } else {
      return null;
    }
  }
}

export default Error;
